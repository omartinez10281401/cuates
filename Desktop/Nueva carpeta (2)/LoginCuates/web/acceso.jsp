<%-- 
    Document   : index
    Created on : 10/10/2014, 10:14:31 PM
    Author     : t@avo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%> 

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>BD CON JSP</title>
    </head>
    <body>
        
        
        <jsp:useBean id="usuario" class="armando.User" scope="request">
            <jsp:setProperty name="usuario" property="*"/>
        </jsp:useBean>
        <%@include file="/WEB-INF/conexion.jspf"%>
        
        <sql:transaction dataSource="${conexion}">
            
            <c:catch var="ex">
                <sql:query var="resultado">
                    SELECT  nombre FROM cuates where nombre='<jsp:getProperty name="usuario" property="nombre"/>' AND password='<jsp:getProperty name="usuario" property="password"/>';
                </sql:query>
           </c:catch>
           <c:choose>
                <c:when test="${not empty ex}">
                 <p>Problema: <c:out value="${ex.message}"/></p>
                </c:when>
                <%-- Si no hubo ninguna excepción --%>
                <c:otherwise>
                    
                    <c:choose>
                        <%-- Si encontró algo --%>
                        <c:when test="${resultado.rowCount>0}">
                         
                             <h1>Welcome to the friends Cave</h1><hr>
                             
                        </c:when>
                        <c:otherwise>
                         <p>Alerta, Intruso</p><hr>
                         <h1>LLamen a los Perros...!!!</h1>
                        </c:otherwise>
                    </c:choose>

                </c:otherwise>
            </c:choose> 
        </sql:transaction> 
    </body>
</html>
